import {Component, Inject, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-dolg-ustvari',
  templateUrl: './dolg-ustvari.component.html',
  styleUrls: ['./dolg-ustvari.component.css']
})
export class DolgUstvariComponent {

  constructor(public dialog: MatDialog, private router: Router) { }

  openDialog(): void {
    let dialogRef = this.dialog.open(UstavriDolgDialogOverviewDialogComponent, {
      width: '750px',
      height: '700px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.router.navigate(['/', { outlets: { dolg: ['dolgovi-od-projekta'] } }]);
    });
  }

}

@Component({
  selector: 'app-dolg-ustvari-dialog',
  templateUrl: './dolg-ustvari.dialog.component.html',
  styleUrls: ['./dolg-ustvari.component.css']
})
export class UstavriDolgDialogOverviewDialogComponent implements OnInit {

  projekt: any;
  dolgForm: FormGroup;
  naziv = '';
  vzrok = '';
  tip = '';
  podtip = '';
  opis = '';
  obseg = '';
  ocena = '';
  kriticnost = '';
  status = '';
  id_projekt = '';
  clan = '';
  clan1 = '';
  clan2 = '';
  clan3 = '';
  clan4 = '';
  clan5 = '';
  clan6 = '';
  clan7 = '';
  clan8 = '';
  clan9 = '';

  tip1 = false;
  tip2 = false;
  tip3 = false;
  tip4 = false;
  tip5 = false;
  tip6 = false;
  tip7 = false;
  tip8 = false;
  tip9 = false;
  tip10 = false;
  tip11 = false;
  tip12 = false;
  tip13 = false;
  tip14 = false;
  tip15 = false;

  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder,
              public dialogRef: MatDialogRef<UstavriDolgDialogOverviewDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.router.navigate(['/', { outlets: { dolg: ['nalaganje'] } }]);
    this.getProjektPodrobnosti(this.router.url.slice(21, 45));
    this.dolgForm = this.formBuilder.group({
      'naziv' : [null, Validators.required],
      'vzrok' : [null],
      'tip' : [null],
      'podtip' : [null],
      'opis' : [null, Validators.required],
      'obseg' : [null, Validators.required],
      'ocena' : [null, Validators.required],
      'kriticnost' : [null, Validators.required],
      'status' : [null, Validators.required],
      'id_projekt' : this.router.url.slice(21, 45),
      'clan' : [null],
      'clan1' : [null],
      'clan2' : [null],
      'clan3' : [null],
      'clan4' : [null],
      'clan5' : [null],
      'clan6' : [null],
      'clan7' : [null],
      'clan8' : [null],
      'clan9' : [null]
    });
  }

  getProjektPodrobnosti(id) {
    this.api.getProjekt(id)
      .subscribe(data => {
        // console.log(data);
        this.projekt = data;
      });
  }

  onFormSubmit(form: NgForm) {
    this.api.postDolg(form)
      .subscribe(res => {
        const id = res['_id'];
        this.dialogRef.close();
      }, (err) => {
        console.log(err);
      });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  tip_1(): void {
    this.tip1 = true;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
}
  tip_2(): void {
    this.tip1 = false;
    this.tip2 = true;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_3(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = true;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_4(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = true;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_5(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = true;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_6(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = true;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_7(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = true;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_8(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = true;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_9(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = true;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_10(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = true;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_11(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = true;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_12(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = true;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_13(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = true;
    this.tip14 = false;
    this.tip15 = false;
  }
  tip_14(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = true;
    this.tip15 = false;
  }
  tip_15(): void {
    this.tip1 = false;
    this.tip2 = false;
    this.tip3 = false;
    this.tip4 = false;
    this.tip5 = false;
    this.tip6 = false;
    this.tip7 = false;
    this.tip8 = false;
    this.tip9 = false;
    this.tip10 = false;
    this.tip11 = false;
    this.tip12 = false;
    this.tip13 = false;
    this.tip14 = false;
    this.tip15 = true;
  }

}
