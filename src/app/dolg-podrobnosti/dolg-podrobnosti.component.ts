import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-dolg-podrobnosti',
  templateUrl: './dolg-podrobnosti.component.html',
  styleUrls: ['./dolg-podrobnosti.component.css']
})
export class DolgPodrobnostiComponent implements OnInit {

  dolg: any;
  tip_ocene: string;

  constructor(private route: ActivatedRoute, private api: ApiService, private router: Router, private _location: Location) { }

  ngOnInit() {
    this.getDolgPodrobnosti(this.route.snapshot.params['id']);
  }

  getDolgPodrobnosti(id) {
    this.api.getDolg(id)
      .subscribe(data => {
        this.dolg = data;
        this.api.getProjekt(data.id_projekt).subscribe(res => {
          this.tip_ocene = res.tip_ocena;
        });
      });
  }

  deleteDolg(id) {
    this.api.deleteDolg(id)
      .subscribe(res => {
        this.router.navigate(['/', { outlets: {primary: ['projekti'], dolg: ['dolgovi'] } }]);
        }, (err) => {
          console.log(err);
        }
      );
  }

  backClicked() {
    this._location.back();
  }
}
