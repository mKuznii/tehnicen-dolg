import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DolgPodrobnostiComponent } from './dolg-podrobnosti.component';

describe('DolgPodrobnostiComponent', () => {
  let component: DolgPodrobnostiComponent;
  let fixture: ComponentFixture<DolgPodrobnostiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DolgPodrobnostiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DolgPodrobnostiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
