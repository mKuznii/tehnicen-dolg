interface AuthConfig {
  NAMESPACE: string;
};

export const AUTH_CONFIG: AuthConfig = {
  NAMESPACE: 'https://tehnicen-dolg.com/role'
};
