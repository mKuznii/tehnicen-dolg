import {Component, OnInit, ViewChild} from '@angular/core';
import { ApiService } from '../api.service';
import { DataSource } from '@angular/cdk/collections';
import {MatSort, MatTableDataSource} from '@angular/material';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'app-dolg',
  templateUrl: './dolg.component.html',
  styleUrls: ['./dolg.component.css']
})
export class DolgComponent implements OnInit {

  displayedColumns = ['naziv', 'datum', 'kriticnost', 'status'];
  dataSource: any;
  profile: any;
  loadCompleted = false;

  constructor(private api: ApiService, public auth: AuthService) { }

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.api.getDolgovi()
    .subscribe(res => {
      if (this.auth.userProfile) {
        this.profile = this.auth.userProfile;
        console.log("stored profile email: " + this.profile.email);
        this.processData(res);
      } else {
        this.auth.getProfile((err, profile) => {
          this.profile = profile;
          console.log("profile email: " + this.profile.email);
          this.processData(res);
        });
      }
    }, err => {
      console.log(err);
      this.getDolgs();
    });
    //this.getDolgs();
  }

  ngOnDestroy() {
    this.loadCompleted = false;
  }

  processData(res: any): void {
    let podatki = [];
          for (let i in res) {
            podatki = podatki.concat([res[i]]);
          }
          let email: string;
          this.dataSource = new MatTableDataSource(podatki);
          email = this.profile.email.trim();
          email = email.toLowerCase();
          this.dataSource.filter = email;
          this.dataSource.sort = this.sort;
          this.loadCompleted = true;
  }

  getDolgs(): void {
    this.api.getDolgovi()
      .subscribe(res => {
        this.getProfile(res);
      }, err => {
        console.log(err);
        this.getDolgs();
      });
  }

  getProfile(res: any): void {
    this.auth.getProfile((err, profile) => {
      if(err){
        console.log("my error:" + err);
        this.getProfile(res);
      }else{
        let podatki = [];
        for (let i in res) {
          podatki = podatki.concat([res[i]]);
        }
        let email: string;
        this.dataSource = new MatTableDataSource(podatki);
        email = profile.email.trim();
        email = email.toLowerCase();
        this.dataSource.filter = email;
        this.dataSource.sort = this.sort;
      }
    });
  }
}
