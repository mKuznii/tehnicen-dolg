import { Component, OnInit } from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'app-slika-profil',
  templateUrl: './slika-profil.component.html',
  styleUrls: ['./slika-profil.component.css']
})
export class SlikaProfilComponent implements OnInit {

  profile: any;
  slika: any;
  ime: any;

  constructor(public auth: AuthService, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    if (this.auth.userProfile) {
      this.profile = this.auth.userProfile;
    } else {
      this.auth.getProfile((err, profile) => {
        this.profile = profile;
        this.slika = this.sanitizer.bypassSecurityTrustResourceUrl(this.profile.picture);
        this.ime = this.profile.name;
      });
    }
  }

}
