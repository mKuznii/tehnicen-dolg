import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiService} from '../api.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-projekt-podrobnosti',
  templateUrl: './projekt-podrobnosti.component.html',
  styleUrls: ['./projekt-podrobnosti.component.css']
})
export class ProjektPodrobnostiComponent implements OnInit {

  projekt: any;

  dolgovi = [];

  constructor(private route: ActivatedRoute, private api: ApiService, private router: Router, public snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.getProjektPodrobnosti(this.route.snapshot.params['id']);
    this.api.getDolgovi()
      .subscribe(res => {
        console.log(res);
        for (let i in res) {
          if (res[i].id_projekt === this.router.url.slice(21, 45)) {
            var dolg = {naziv: res[i].naziv};
            this.dolgovi.push(dolg);
          }
        }
      }, err => {
        console.log(err);
      });
  }

  getProjektPodrobnosti(id) {
    this.api.getProjekt(id)
      .subscribe(data => {
        console.log(data);
        this.projekt = data;
      });
  }

  deleteProjekt(id) {
    console.log(this.dolgovi.length);
    if (this.dolgovi.length > 0) {
      this.snackBar.openFromComponent(IzbrisOnemogocenComponent, {
        duration: 3000,
      });
    } else {
      this.api.deleteProjekt(id)
        .subscribe(result => {
            this.router.navigate(['/', { outlets: {primary: ['projekti'], dolg: ['dolgovi'] } }]);
          }, (err) => {
            console.log(err);
          }
        );
    }
  }
}

@Component({
  selector: 'app-izbris-onemogocen',
  templateUrl: './izbris-onemogocen.component.html',
  styleUrls: ['./izbris-onemogocen.component.css']
})
export class IzbrisOnemogocenComponent {
}
