import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjektUstvariComponent } from './projekt-ustvari.component';

describe('ProjektUstvariComponent', () => {
  let component: ProjektUstvariComponent;
  let fixture: ComponentFixture<ProjektUstvariComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjektUstvariComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjektUstvariComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
